import '../public/style/index.less'
import React from 'react'
import ReactDom from 'react-dom'
import HelloWorld from './component/index'

ReactDom.render(<HelloWorld />, document.getElementById('container'))