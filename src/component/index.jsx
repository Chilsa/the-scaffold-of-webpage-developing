import React from 'react'

export default class HelloWorld extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return <div>Hello world!</div>
    }
}